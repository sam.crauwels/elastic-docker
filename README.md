# Elastic on Docker

This project aims to give some ready-to-use Docker deployments to setup a Elastic stack (Kibana - Elasticsearch - Fleet) for different purposes on a Linux machine. I'm only testing this on Docker on Debian.

## Requirements

- A machine with Docker and Docker Compose installed. [Link](https://docs.docker.com/compose/install/)
- This Git repository cloned.
- On the Linux machine, set the `vm.max_map_count` to 262144. [Link](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_linux)

## Usage

Go into the directory of the deployment configuration you need, adjust the setting in the `.env` file to your likings and run:
`$ docker compose up -d`

This will start the containers in the background. After a while, the containers will be available.